"""
Bot helping to track expenses during sh0pping on the market in Cambodia
supports two currencies = USD and KHR
amount considered as amount is USD in case of amount<100 or there is '$' sign in front or after amount
(no space between), otherwise amount considered as amount in KHR

modes:
    market - tracking expenses
    counter - helping to calculate amount of cash in the pocket in two currencies
"""

import telebot
from collections import defaultdict
from decouple import config
import logging
import os
import redis

# CONSTANTS:
# loging
LOG_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)),'logs')
LOG_FILENAME = 'marketbot.log'
LOG_LEVEL = logging.DEBUG
# telegram bot token
TOKEN = config('TOKEN')
#Exchange rate
RATE = config('RATE', cast=int)
# Redis DB setup
REDIS_HOST = config('REDIS_HOST')
REDIS_PORT = config('REDIS_PORT', cast=int)
REDIS_DB = config('REDIS_DB', cast=int)

# modes:
FREE, MARKET, COUNTER = range(3)

#setup logging
filename = os.path.join(LOG_DIR, LOG_FILENAME)
# if not os.path.isdir(LOG_DIR):
#     os.mkdir(LOG_DIR)
logging.basicConfig(filename=filename, format='[%(asctime)s] %(name)s - %(levelname)s: %(message)s', filemode='a', level=LOG_LEVEL)


# Here we go
logging.debug(f"Starting bot")
bot = telebot.TeleBot(TOKEN, threaded=False)
r = redis.Redis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB, decode_responses=True)
user_mode = defaultdict(lambda: FREE)


def get_keyboard(user):
    """
    Define keyboard to be shown to user depending of current mode
    """
    mode = user_mode[user]
    if mode == FREE:
        keyboard = telebot.types.ReplyKeyboardMarkup(row_width=2)
        buttons = []
        buttons.append(telebot.types.KeyboardButton('/market'))
        buttons.append(telebot.types.KeyboardButton('/counter'))
        keyboard.add(*buttons)
        keyboard.resize_keyboard = True
    else:
        keyboard = telebot.types.ReplyKeyboardMarkup(row_width=1)
        buttons = []
        buttons.append(telebot.types.KeyboardButton('/end'))
        keyboard.add(*buttons)
        keyboard.resize_keyboard = True
    return keyboard

def get_current_status(user):
    stat = r.hgetall(user)
    usd_ = stat.get('usd') or 0
    khr_ = stat.get('khr') or 0
    cur1 = int(usd_)
    cur2 = int(khr_)
    return cur1, cur2

@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    """
    Answer to standard commands: /start, /help
    """
    bot.reply_to(message, f'Hello, \n'
    f'With help of this bot you can easily track your expenses during shopping on the market \n'
    f"Amount considered as amount is USD in case of amount<100 or there is '$' sign in front or after amount " 
    f"(no space between), otherwise amount considered as amount in KHR\n"
    f'Available commands: \n'
    f'/market   Market mode - tracking expenses \n'
    f'/counter  Calculate amount of cash in the pocket \n'
    f'/help     This message \n',
    reply_markup=get_keyboard(message.chat.id))

@bot.message_handler(commands=['market'])
def market_mode(message):
    """
    Switching to a market mode
    """
    user_mode[message.chat.id] = MARKET
    r.delete(message.chat.id)
    bot.reply_to(message, f'You are in a market mode', reply_markup=get_keyboard(message.chat.id))

@bot.message_handler(commands=['end'])
def mode_end(message):
    """
    finalization of modes
    calculate final amount, convert if to USD and send it to user
    """
    cur_mode = user_mode[message.chat.id]
    user_mode[message.chat.id] = FREE
    if cur_mode == MARKET:
        title = 'Your total expense is:'
    else:
        title = 'Your total amount is:'
    # stat = r.hgetall(message.chat.id)
    # usd_ = stat.get('usd') or 0
    # khr_ = stat.get('khr') or 0
    # usd_ = int(usd_)
    # khr_ = int(khr_)
    cur1, cur2 = get_current_status(message.chat.id)
    msg = f"{title} \n {'USD':^15} {'KHR':^15} {'USD Total':^15} \n {cur1:^15,.2f} {cur2:^15,.2f} {cur1+cur2/RATE:^15,.2f}"
    bot.send_message(message.chat.id, msg, reply_markup=get_keyboard(message.chat.id))

@bot.message_handler(func=lambda message: user_mode[message.chat.id] == MARKET)
def add_expenses(message):
    """
    For users in market mode: adding expenses
    expenses can be set of integers divided by space or comma
    positive value is expense, negative is income

    USD - amount between -100 and 100 or first digit is $ or last digit is $
    KHR - all others

    """
    usd, khr = 0, 0
    amts = message.text.split(' ')
    for amt in amts:
        if amt[0] == '-':
            multiplier = -1
            amt = amt[1:100]
        else:
            multiplier = 1
        if amt[0] == '$':
            usd += int(amt[1:100]) * multiplier
        elif amt[-1] == '$':
            usd += int(amt[-100:-1]) * multiplier
        elif amt.isnumeric():
            if int(amt) * int(amt) < 10000:
                usd += int(amt) * multiplier
            else:
                khr += int(amt) * multiplier
        else:
            print(amt)
    if usd != 0:
        r.hincrby(message.chat.id, 'usd', usd)
    if khr != 0:
        r.hincrby(message.chat.id, 'khr', khr)
    if (usd != 0) or (khr != 0):
        cur1, cur2 = get_current_status(message.chat.id)
        msg = f"Your current expenses are: \n {'USD':^15} {'KHR':^15} \n {cur1:^15,.2f} {cur2:^15,.2f}"
    else:
        msg = f'No amounts found {amt}'
    bot.send_message(message.chat.id, msg, reply_markup=get_keyboard(message.chat.id))


@bot.message_handler(commands=['counter'])
def market_mode(message):
    bot.reply_to(message, f'Function is not implemented yet', reply_markup=get_keyboard(message.chat.id))

# running the bot forever (hopefully)
bot.polling(none_stop=True)
